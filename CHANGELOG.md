# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.5.0] - 2025-02-27

## Changed

- Precommit and some other project infra
- Dropped AA3 and added py313 testing

### Fixed

- Moment.js Localisation

### Added

- Chart.JS

## [0.4.1] - 2024-05-11

### Fixed

- Integrity Hashes for a few files

## [0.4.0] - 2024-03-16

Match AA v4.x

## [0.3.4] - 2023-10-08

Maintenance Release

## [0.3.3 PreRelease] - 2021-12-28

- Added missing jQuery-UI images. @ppfeufer

## [0.3.2 PreRelease] - 2021-10-30

- Added jQuery-UI 1.12.1

## [0.3.1 PreRelease] - 2021-10-30

Compile fresh CSS using --advanced + aa-gdpr local resources as per AA 2.9.3.

## [0.3.0 PreRelease] - 2021-10-30

## Changed

Adds many updated package versions in line with AA 2.9.3

## [0.2.5 PreRelease] - 2021-03-26

### Changed

Adds/Updates to Clipboard.js 0.2.8

## [0.2.4 PreRelease] - 2020-09-11

### Fixed

Add jquery-visibility for Alliance Auth v2.8.2 (@ppfeufer)

## [0.2.3 PreRelease] - 2020-09-11

### Fixed

Django-ESI 2.0.5 Support

## [0.2.2 PreRelease] - 2020-09-11

### Fixed

Django 3.1 Support

## [0.2.1 PreRelease] - 2020-09-11

### Fixed

Moment-JS Localisation now loads in the right order and properly passes Language_Code (Credit: ppfeufer)

## [0.2.0 PreRelease] - 2020-08-27

### Added

Django-ESI Template overrides

## [0.1.1 PreRelease] - 2020-08-27

### Fixed

Deliver x-editable for bootstrap 3, not 2.

## [0.1.0 PreRelease] - 2020-08-27

### Added

Local StaticFile options for CSS JS and Font delivery, Based on AA 2.7.4. Requires 2.7.unreleased for some resource to be properly overridden

### Changed

### Fixed
